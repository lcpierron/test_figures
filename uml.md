# Diagramme de classes pour la branche `figures`

```PlantUML
@startuml
Class Canvas

Class Circle {
    - int diameter
    - int xPosition
    - int yPosition
    - String color
    - boolean isVisible
    + makeVisible()
    + makeInvisible()
    + moveRight()
    + moveLeft()
    {method} Autres méthodes... 
}

Class Square {
    - int size
    - int xPosition
    - int yPosition
    - String color
    - boolean isVisible
    + makeVisible()
    + makeInvisible()
    + moveRight()
    + moveLeft()
    {method} Autres méthodes... 
}

Class Triangle {
    - int height
    - int width
    - int xPosition
    - int yPosition
    - String color
    - boolean isVisible
    + makeVisible()
    + makeInvisible()
    + moveRight()
    + moveLeft()
    {method} Autres méthodes... 
}

Class Person {
    - int height
    - int weight
    - int xPosition
    - int yPosition
    - String color
    - boolean isVisible
    + makeVisible()
    + makeInvisible()
    + moveRight()
    + moveLeft()
    {method} Autres méthodes... 
}
 
Canvas <.. Circle : s'affiche dans
Canvas <.. Square : s'affiche dans
Canvas <.. Triangle : s'affiche dans
Canvas <.. Person : s'affiche dans
@enduml
```

# Diagramme de classes pour la branche `house`


